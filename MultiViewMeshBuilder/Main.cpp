#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>

#include <opencv/cv.h>
#include <iostream>
#include <vector>

#define DEBUG

using namespace cv;
using namespace std;

struct Descriptor {
	Point3f position;
	HOGDescriptor descriptor;
};

const char* DEBUG_WINDOW = "Display window";
vector <Descriptor> getPoints(Mat, Mat);

int main(int argc, char* argv[]) {
	if(false && argc != 3) {
		cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		return -1;
	}

	char* image0Filename = "c:\\tmp\\img0.jpg"; // argv[1]
	char* image1Filename = "c:\\tmp\\img1.jpg"; // argv[2]

	Mat image0, image1;
	image0 = imread(image0Filename, IMREAD_GRAYSCALE); // Read the file
	image1 = imread(image1Filename, IMREAD_GRAYSCALE);

	if(!image0.data || !image1.data) { // Check for invalid input
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	namedWindow(DEBUG_WINDOW, WINDOW_AUTOSIZE); // Create a window for display.
	//imshow("Display window", image0); // Show our image inside it.
	getPoints(image0, image1);

	waitKey(0); // Wait for a keystroke in the window
	return 0;
}

/** getPoints
 Match points from uncalibrated camera.
 */
vector <Descriptor> getPoints(Mat image0, Mat image1) {
	const int blockSize = 2;
	const int kSize = 3;
	const double kValue = 0.04;
	unsigned short threshold = 256;
	const double thresholdDecay = 0.9;

	std::vector <Descriptor> triangulatedPoints;

	// Extract features
	std::vector <vector<KeyPoint>> pointSets;
	Mat images[] = {image0, image1};
	for(int index=0; index < 2; index++) {
		vector <KeyPoint> points;
		Mat destination, normalized, absResponse;
		destination = Mat::zeros(images[index].size(), CV_32FC1);
		cornerHarris(images[index], destination, blockSize, kSize, kValue, BORDER_DEFAULT);
		normalize(destination, normalized, 0.0, 255.0, NORM_MINMAX, CV_32FC1, Mat());
		convertScaleAbs(normalized, absResponse);

		while(points.size() < 16) {
			points.empty();
			threshold = (int)(threshold*thresholdDecay);
			for(int j=0; j < absResponse.rows; j++) {
				for(int i=0; i < absResponse.cols; i++) {
					if((unsigned short)absResponse.at<unsigned char>(Point(i, j)) > threshold) {
#ifdef DEBUG
						circle(absResponse, Point(i, j), 10, Scalar(0), 2, 8, 0);
						cout << "Got response of " << (int)absResponse.at<unsigned char>(Point(i, j)) << " at " << i << ", " << j << endl;
#endif
						points.push_back(KeyPoint(i, j, kSize));
					}
				}
			}
			if(threshold <= 1) {
				// Problem.  We can't find any points.
				cerr << "Unable to detect points in image " << index << endl;
				return vector <Descriptor>();
			}
		}

		imshow(DEBUG_WINDOW, absResponse);

		pointSets.push_back(points);
	}

	// Extract descriptors
	FREAK descriptor;

	// Match descriptors
	BruteForceMatcher <Hamming> matcher;

	// Find fundamental matrix
	// Extract projection matrices (3x4) from F.
	// Triangulate features
	// Process and iterate

	return triangulatedPoints;
}
